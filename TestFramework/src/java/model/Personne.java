/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import ModelView.ModelView;
import annotation.URLDynamique;
import java.util.HashMap;

/**
 *
 * @author USER
 */
public class Personne {

    private String nom;
    private String prenom;
    private int age;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        System.out.println("Set age");
        this.age = age;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @URLDynamique(url = "VoirInfo")
    public ModelView info() {
        ModelView mdl = new ModelView();
        HashMap<String, Object> attributs = new HashMap<String, Object>();
        attributs.put("name", "Hello  "+this.getNom());
        attributs.put("age", this.getAge());

        mdl.setData(attributs);
        mdl.setPage("personne.jsp");
        return mdl;
    }

}
