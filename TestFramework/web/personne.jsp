<%-- 
    Document   : index
    Created on : 2 nov. 2022, 18:42:10
    Author     : USER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome</title>
    </head>
    <body>
        <h1><% out.print(request.getAttribute("name"));%></h1> 
        <p><% out.print(request.getAttribute("age"));%></p>
    </body>
</html>
